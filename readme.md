# kaffinate

kaffinate is a kubernetes sdk, designed for use by a working dev/sre.

kaffinate thus _appears_ as if it were a thin wrapper over common
Kubernetes objects.

What kaffinate effects, however, is to build an integrated object mesh
around Kubernetes. Where Kubernetes, by default, is a _collection_ of
useful objects, and `helm` templates them into a stringly typed
agglutination, and `pulumi` and `terraform` seek to reconcile a
disparate group of infrastructure resources into a given state - the
kaffinate sdk seeks to intelligbly create a working object system where
each Kubernetes object is placed within a reasonable container in the
system and then the Kubernetes API is directly called.

It is critically important to understand that `kaffinate` is an *opinionated* platform.
`kaffinate` is designed for use in conventional cloud-native compute applications. 

Some rules of how `kaffinate` builds an integrated system. More exist, more will come.

* Each deployment can have an HPA configured out of the box.
* Deployments, Daemonsets, and Statefulsets are all subclassed under PodManagers.
* Each PodManager _always_ has a Service with one port. An Ingress, if one is desired, will be bound to that port.
* Each Secret and ConfigMap attached to a PodManager will be projected into a volume for service reading.
* Each PodManager will have a serviceaccount uniquely created for it.

New rules and features can be added, but strictly under guidelines: 
- does this work in practice, for cloud-native applications?
  - (cloud native can be loosely read as 12-factor)
- It is not sufficient to exist in Kubernetes. 
- It should work reliably and predictably, rather than in a configurable & flexible way.

In other words: `kaffinate` is not a general purpose Kubernetes SDK (that exists). 


The current deployment of `kaffinate` works for rendering YAML objects. Direct API access to follow.

# Usage.

In your favorite JVM language, pull in the `kaffinate` jar. Kaffinate is written in Java 21 (if you want it to 
generate lower-than-21 byte code, patches accepted.
