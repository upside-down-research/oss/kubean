All contributions are welcome.

Contributions remain copyright of the individual who made them, under the select project license at time of contribution.

Unit tests are hoped for.

No functionality should be broken without a very thorough discussion.


