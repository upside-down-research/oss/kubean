ASSET_DIRS = $(shell find src/ -type d)
ASSET_FILES = $(shell find src/ -type f -name '*.java')

.makefile.compile: src/ $(ASSET_DIRS) $(ASSET_FILES)
	mvn compile
	touch .makefile.compile


compile: .makefile.compile

.makefile.package: .makefile.compile
	mvn package
	touch .makefile.package

package: .makefile.compile

clean:
	rm -f .makefile.compile
	rm -f .makefile.package
	mvn clean

run: .makefile.package
	@java --enable-preview -jar target/kaffinate-1.0-SNAPSHOT-jar-with-dependencies.jar
