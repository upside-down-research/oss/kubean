package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Daemonset extends PodManager implements Renderable {

    public Daemonset(String name) {
        super(name);
    }

    @Override
    public String render(RuntimeOptions options) throws KaffinateRootException {

        List<String> rendered = new ArrayList<>(super.renderChain(options));

        V1DaemonSet deploy = synthesizeObject(options);

        rendered.add(Yaml.dump(deploy));

        return String.join("\n---\n", rendered);
    }

    private V1DaemonSet synthesizeObject(RuntimeOptions options) {

        Map<String, String> podLabels = options.mutableLabels();

        Map<String, String> objectLabels = options.allLabels();

        V1PodTemplateSpec podTemplateSpec = getV1PodTemplateSpec(podLabels, options);

        V1DaemonSet daemonSet = new V1DaemonSet();
        daemonSet.setApiVersion("apps/v1");
        daemonSet.setKind("DaemonSet");
        daemonSet.setMetadata(
                new V1ObjectMeta()
                        .namespace(options.namespace())
                        .name(name)
                        .labels(objectLabels)
                        .annotations(this.managerAnnotations));
        V1DaemonSetSpec spec = new V1DaemonSetSpec()
                .selector(new V1LabelSelector().matchLabels(options.immutableLabels(this.managerLabels)))
                .template(podTemplateSpec);
        daemonSet.setSpec(spec);

        return daemonSet;
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }
}
