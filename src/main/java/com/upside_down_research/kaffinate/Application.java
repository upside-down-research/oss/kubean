package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


// An Application is the top-level definition of a deployable object
// It is parameterized by the namespace, as well as an optional prefix for objects
public class Application implements Renderable {

    // Podmanagers include daemonsets, deploys, statefulsets, etc
    List<PodManager> podManagers;
    List<ConfigMap> configMaps;

    List<Secret> secrets;

    String applicationName;

    public Application(String name) {
        this.applicationName = name;
        this.podManagers = new ArrayList<>();
        this.configMaps = new ArrayList<>();
        this.secrets = new ArrayList<>();
    }

    public String name() {
        return this.applicationName;
    }

    public Application withPodManagers(List<PodManager> deploys) {
        this.podManagers = deploys;
        return this;
    }
    public Application withPodmanager(PodManager deploy) {
        this.podManagers.add(deploy);
        return this;
    }

    public Application withConfigMaps(List<ConfigMap> configMaps) {
        this.configMaps = configMaps;
        return this;
    }

    public Application addSecret(Secret secret) {
        this.secrets.add(secret);
        return this;
    }


    @Override
    public String render(RuntimeOptions options) throws KaffinateRootException {
        java.util.logging.Logger.getLogger("org.yaml.snakeyaml.introspector").setLevel(java.util.logging.Level.SEVERE);
        List<String> rendered = new ArrayList<>();

        for (ConfigMap configMap : configMaps){
            rendered.add(configMap.render(options));
        }
        for (Secret secret : secrets){
            rendered.add(secret.render(options));
        }

        for (PodManager deploy : podManagers){
            deploy.memo(configMaps, secrets);
            rendered.add(deploy.render(options));
            deploy.clearMemo();
        }
        return String.join("\n---\n", rendered);
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException, KaffinateRootException {
        for (PodManager pm : podManagers){
            pm.create(options);
        }

        for (ConfigMap configMap : configMaps){
            configMap.create(options);
        }
    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {
        for (PodManager pm : podManagers){
            pm.delete(options);
        }
        for (ConfigMap configMap : configMaps){
            configMap.delete(options);
        }
    }
}
