package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.models.V1Container;

import java.util.List;

public class InitContainer {
    String name;
    String image;
    List<String> args;

    public V1Container synthesize(RuntimeOptions options) {
        return new V1Container()
                .name(name)
                .image(image)
                .args(args);
    }
}
