package com.upside_down_research.kaffinate.driver;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

final public class Log {
    public static void info(String message){
        LocalDateTime date = LocalDateTime.now();
        String text = date.format(DateTimeFormatter.ISO_DATE_TIME);
        System.err.println(text + " " + message);
    }
}
