package com.upside_down_research.kaffinate.driver;

import com.upside_down_research.kaffinate.*;

import java.util.Map;

public class Main {
    public static void main(String args[]) throws KaffinateRootException {
        Log.info("Starting application");

        PodManager statefulSet = new StatefulSet("test-statefulset")
                .withReplicas(3)
                .withImage( "httpd:2.4")
                .withPodPort("http", 80)
                .withServicePort("http", 9090);

        PodManager pm = new Daemonset("test-daemonset")
                .withImage("httpd:2.4")
                .withPodPort("http", 80)
                .withServicePort("http", 1337);


        ConfigMap cm1 = new ConfigMap("test-configmap", new Text(Map.of("entry", "hello world")));
        ConfigMap cm2 = new ConfigMap("test-configmap2", new Binary(Map.of("otherentry", "hello world".getBytes())));



        // TODO: bind this into a PodManager object but get it plumbed through the PodManager api
        Ingress ingress = new Ingress("test-ingress",
                Map.of(),
                "http",
                "/",
                "example.com");

        DockerConfigSecret dcs = new DockerConfigSecret("docker", "test-registry", "test-username", "test-password", "example.com");

        PodManager deploy = new Deployment("test-deploy")
                .enableHpa("cpu", 50)
                .enableHpa("memory", 80)
                .withReplicas(3)
                .withImage("httpd:2.4")
                .withPodPort("http", 80)
                .withServicePort("http", 8080)
                .withPullSecrets(dcs);
        deploy.withIngress(ingress);
        Application app = new Application("test-app")
                .withPodmanager(deploy);
        app.addSecret(dcs);
        //app.withConfigMaps(List.of(cm1, cm2));

        Secret s = new Secret("test-secret", Map.of("password", "super secret".getBytes()));
        //app.addSecret(s);
        java.util.logging.Logger.getLogger("org.yaml.snakeyaml.introspector").setLevel(java.util.logging.Level.SEVERE);
        System.out.println(app.render(new RuntimeOptions("default",
                app.name(),
                Map.of("appVersion", "1.0.0"),
                Map.of("iteration", "1"))));
    }
}
