package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1ServiceAccount;
import io.kubernetes.client.util.Yaml;

import java.io.IOException;
import java.util.HashMap;

class ServiceAccount implements Renderable {
    private final String name;

    public ServiceAccount(String name) {
        this.name = name;
    }

    V1ServiceAccount synthesize(RuntimeOptions options) {
        V1ServiceAccount sa = new V1ServiceAccount();
        sa.apiVersion("v1");
        sa.kind("ServiceAccount");

        HashMap<String, String> salabels = new HashMap<>();
        salabels.putAll(options.allLabels());

        sa.metadata(new V1ObjectMeta().name(name).namespace(options.namespace()));

        return sa;
    }

    @Override
    public String render(RuntimeOptions options) {
        V1ServiceAccount sa = synthesize(options);
        return Yaml.dump(sa);

    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {
        

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }
}