package com.upside_down_research.kaffinate;

import java.util.HashMap;
import java.util.Map;

public class RuntimeOptions {

    /***
     * immutable_labels should not be mutable between runs. Example, name of app.
     */
    protected final Labels immutable_labels;
    /**
     * mutable_labels can be changed between runs. Example, version of app.
     */
    protected final Labels mutable_labels;
    private final String namespace;
    private final String name;

    public RuntimeOptions(String namespace) {
        this.namespace = namespace;
        this.immutable_labels = new Labels();
        this.mutable_labels = new Labels();
        this.name = "unset";
    }

    public RuntimeOptions(String namespace, String name, Map<String, String> labels, Map<String, String> mutable_labels) {
        this.namespace = namespace;
        this.immutable_labels = new Labels(labels);
        this.mutable_labels = new Labels(mutable_labels);
        this.name = name;
    }

    public String name() {
        return this.name;
    }

    public String namespace() {
        return namespace;
    }

    public Map<String, String> allLabels() {
        Labels merged = this.immutable_labels.merge(this.mutable_labels);
        return merged.asMap();
    }

    public Map<String, String> allLabels(Map<String, String> localLabels) {
        return this.immutable_labels
                .merge(this.mutable_labels)
                .merge(new Labels(localLabels))
                .asMap();
    }

    public Map<String, String> immutableLabels() {
        return immutable_labels.asMap();
    }

    public Map<String, String> immutableLabels(Map<String, String> localLabels) {
        return this.immutableLabels(new Labels(localLabels));
    }

    public Map<String, String> immutableLabels(Labels localLabels) {
        return this.immutable_labels.merge(localLabels).asMap();
    }

    public Map<String, String> mutableLabels() {
        return mutable_labels.asMap();
    }

    public Map<String, String> mutableLabels(Map<String, String> localLabels) {
        return this.mutableLabels(new Labels(localLabels));
    }

    public Map<String, String> mutableLabels(Labels localLabels) {
        return mutable_labels.merge(localLabels).asMap();
    }

}
