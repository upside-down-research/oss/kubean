package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.util.Yaml;

import java.io.IOException;
import java.util.HashMap;

public class ConfigMap implements Renderable, Projectable {
    private final String configMapName;
    private final ConfigMapDataclass data;

    public ConfigMap(String name, ConfigMapDataclass data) {
        this.configMapName = name;
        this.data = data;
    }

    public String name() {
        return this.configMapName;
    }

    @Override
    public String render(RuntimeOptions options) {

        V1ConfigMap configMap = new V1ConfigMap();
        configMap.setApiVersion("v1");
        configMap.setKind("ConfigMap");
        configMap.setMetadata(new V1ObjectMeta().namespace(options.namespace()).name(configMapName).labels(options.allLabels()));
        configMap.setImmutable(false);

        switch (this.data) {
            case Binary binary -> {
                configMap.setBinaryData(binary.get());

            }
            case Text text -> {
                configMap.setData(text.get());
            }
        }

        return Yaml.dump(configMap);
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }
}
