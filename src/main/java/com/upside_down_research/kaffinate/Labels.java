package com.upside_down_research.kaffinate;

import java.util.HashMap;
import java.util.Map;


/**
 * Labels is a simple wrapper around a map of strings to strings. It is used to
 * set up an object for reasonability for lebels on Kubernetes objects.
 */
public class Labels {
    private final Map<String, String> labels;

    public Labels() {
        this.labels = new HashMap<>();
    }

    public Labels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> asMap() {
        return this.labels;
    }

    public Labels withLabel(String key, String value) {
        this.labels.put(key, value);
        return this;
    }

    public Map<String, String> merge(Map<String, String> other) {
        Map<String, String> merged = new HashMap<>(this.labels);
        merged.putAll(other);
        return merged;
    }

    public Labels merge(Labels other) {
        Labels merged = new Labels();
        merged.labels.putAll(this.labels);
        merged.labels.putAll(other.labels);
        return merged;
    }
}
