package com.upside_down_research.kaffinate;

import java.util.Map;

public final class Text implements ConfigMapDataclass {
    private final Map<String, String> data;

    public Text(Map<String, String> data) {
        this.data = data;
    }

    Map<String, String> get() {
        return data;
    }

}
