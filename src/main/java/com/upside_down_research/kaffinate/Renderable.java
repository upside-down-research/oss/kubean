package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;

import java.io.IOException;

/// a Renderable can be pushed to k8s
public interface Renderable {
    String render(RuntimeOptions options) throws KaffinateRootException;

    void create(RuntimeOptions options) throws KaffinateRootException, IOException, ApiException;

    void delete(RuntimeOptions options) throws IOException, ApiException;
}
