package com.upside_down_research.kaffinate;

public record Pair<T, U>(T first, U second) {
}
