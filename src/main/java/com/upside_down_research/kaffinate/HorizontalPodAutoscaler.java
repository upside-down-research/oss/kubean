package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.AutoscalingV2Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Yaml;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;

public class HorizontalPodAutoscaler implements Renderable {

    private final String name;
    private final String targetName;
    private final Integer targetUtilization;
    private final String type;

    public HorizontalPodAutoscaler(String name, String targetName, String type, Integer targetUtilization) {
        this.name = name;
        this.targetName = targetName;
        this.type = type;
        this.targetUtilization = targetUtilization;
    }

    V2HorizontalPodAutoscaler synthesizeHpa(@NotNull RuntimeOptions options) {
        V2HorizontalPodAutoscaler hpa = new V2HorizontalPodAutoscaler();
        hpa.setApiVersion("autoscaling/v2");
        hpa.setKind("HorizontalPodAutoscaler");
        hpa.setMetadata(new V1ObjectMeta().namespace(options.namespace()).name(name).labels(new HashMap<>()));
        V2HorizontalPodAutoscalerSpec spec = new V2HorizontalPodAutoscalerSpec();
        spec.minReplicas(3);
        spec.maxReplicas(9);
        V2MetricSpec metricsSpec = new V2MetricSpec();
        metricsSpec.setType("Resource");
        metricsSpec.setResource(new V2ResourceMetricSource()
                .name(this.type)
                .target(new V2MetricTarget()
                        .type("Utilization")
                        .averageUtilization(this.targetUtilization)));

        spec.addMetricsItem(metricsSpec);
        spec.scaleTargetRef(new V2CrossVersionObjectReference()
                .apiVersion("apps/v1")
                .kind("Deployment").name(targetName));

        hpa.setSpec(spec);

        HashMap<String, String> label_map = new HashMap<>();
        label_map.putAll(options.allLabels());

        hpa.getMetadata().setLabels(label_map);

        return hpa;

    }


    @Override
    public String render(RuntimeOptions options) {
        V2HorizontalPodAutoscaler hpa = synthesizeHpa(options);
        return Yaml.dump(hpa);
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {
        V2HorizontalPodAutoscaler hpa = synthesizeHpa(options);
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        AutoscalingV2Api api = new AutoscalingV2Api();
        api.createNamespacedHorizontalPodAutoscaler(options.namespace(), hpa, null, null, null, null);
    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {
        V2HorizontalPodAutoscaler hpa = synthesizeHpa(options);
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        AutoscalingV2Api api = new AutoscalingV2Api();
        api.deleteNamespacedHorizontalPodAutoscaler(name, options.namespace(), null, null, null, null, null, null);
    }
}
