package com.upside_down_research.kaffinate;


import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Yaml;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// https://github.com/kubernetes-client/java/blob/master/examples/examples-release-19/src/main/java/io/kubernetes/client/examples/DeployRolloutRestartExample.java#L47
public class Deployment extends PodManager implements Renderable {

    @NotNull
    private int replicas;
    final private List<HorizontalPodAutoscaler> hpas;

    public Deployment(String name) {
        super(name);

        this.replicas = 1;

        this.hpas = new ArrayList<>();

    }

    public Deployment withPort(String http, int i) {
        // ram it straight through, no change.
        withPodPort(http, i);
        withServicePort(http, i);
        return this;
    }


    public Deployment withReplicas(int replicas) {
        this.replicas = replicas;
        return this;
    }

    @Override
    public String render(RuntimeOptions options) throws KaffinateRootException {

        List<String> rendered = new ArrayList<>(super.renderChain(options));

        for (HorizontalPodAutoscaler hpa : this.hpas) {
            V2HorizontalPodAutoscaler h = hpa.synthesizeHpa(options);
            rendered.add(Yaml.dump(h));
        }

        V1Deployment deploy = synthesizeObject(options);

        rendered.add(Yaml.dump(deploy));

        return String.join("\n---\n", rendered);
    }

    @NotNull
    private V1Deployment synthesizeObject(RuntimeOptions options) {

        Map<String, String> podLabels = options.mutableLabels();

        Map<String, String> immutableLabels = options.immutableLabels(this.managerLabels);

        Map<String, String> allLabals = options.allLabels();

        V1PodTemplateSpec podTemplateSpec = getV1PodTemplateSpec(allLabals, options);

        V1DeploymentSpec deploySpec =
                new V1DeploymentSpec()
                        .replicas(this.replicas)
                        .selector(new V1LabelSelector().matchLabels(immutableLabels))
                        .template(podTemplateSpec);

        V1Deployment deploy = new V1Deployment();
        deploy.apiVersion("apps/v1");
        deploy.kind("Deployment");
        deploy.metadata(new V1ObjectMeta()
                .name(name)
                .namespace(options.namespace())
                .labels(immutableLabels)
                .annotations(this.managerAnnotations));

        deploy.spec(deploySpec);
        return deploy;
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {
        for (HorizontalPodAutoscaler hpa : this.hpas) {
            hpa.create(options);
        }

        if (this.hasServiceAccount) {
            ServiceAccount sa = new ServiceAccount(this.serviceAccountName());
            sa.create(options);
        }

        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        AppsV1Api api = new AppsV1Api();
        api.createNamespacedDeployment(options.namespace(), this.synthesizeObject(options), null, null, null, null);
    }

    @Override
    public void delete(RuntimeOptions options) throws ApiException, IOException {
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        AppsV1Api api = new AppsV1Api();
        api.deleteNamespacedDeployment(name, options.namespace(), null, null, null, null, null, null);
    }

    public Deployment enableHpa(String type, Integer targetUtilization) {
        HorizontalPodAutoscaler hpa = new HorizontalPodAutoscaler(name + "-hpa-" + type,
                name, type, targetUtilization);
        hpas.add(hpa);
        return this;
    }
}
