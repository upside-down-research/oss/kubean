package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.*;

/**
 * Ingresses have the most annotation-heavy API of any object
 * They also tend to be heavily variable depending on cluster.
 * <p>
 * While they nominally should be attached to a PodMaaager, meaningful parts of their API
 * need to be plumbed in.
 */
public class Ingress implements Renderable {

    private final String name;
    private final Map<String, String> annotations;
    private final String path;
    private final String host;
    private final Optional<String> ingressClassName;

    // specific port name associated with the service.
    // services can have multiple ports
    private final String portName;
    private Optional<Service> associatedService;

    public Ingress(String name, Map<String, String> annotations, String portName, String path, String host) {
        this.name = name;
        this.annotations = annotations;
        this.path = path;
        this.host = host;
        this.portName = portName;
        this.ingressClassName = Optional.empty();
        this.associatedService = Optional.empty();
    }

    public void memoService(Service service) {
        this.associatedService = Optional.of(service);
    }

    public void clearMemoService() {
        this.associatedService = Optional.empty();
    }

    @Override
    public String render(RuntimeOptions options) throws KaffinateRootException {
        V1Ingress ingress = synthesize(options);
        return Yaml.dump(ingress);
    }

    @NotNull
    private V1Ingress synthesize(RuntimeOptions options) throws MisconfiguredIngressException {
        HashMap<String, String> label_map = new HashMap<>();

        label_map.putAll(options.allLabels());

        V1Ingress ingress = new V1Ingress()
                .apiVersion("networking.k8s.io/v1")
                .kind("Ingress")
                .metadata(new V1ObjectMeta()
                        .name(name)
                        .namespace(options.namespace())
                        .labels(label_map)
                        .annotations(this.annotations));


        if (associatedService.isEmpty()) {
            throw new RuntimeException("Ingress " + name + " has no associated service");
        }


        Optional<Integer> port = associatedService.get().getFrontendPort(this.portName);
        if (port.isEmpty()) {
            throw new MisconfiguredIngressException("Ingress " + name + " has no port " + this.portName + " in associated service");
        }

        Integer portNumber = port.get();

        List<V1IngressRule> rules = new ArrayList<>();
        rules.add(new V1IngressRule()
                .host(host)
                .http(new V1HTTPIngressRuleValue()
                        .paths(Collections.singletonList(new V1HTTPIngressPath()
                                .path(path)
                                .pathType("Prefix")
                                .backend(new V1IngressBackend()
                                        .service(new V1IngressServiceBackend()
                                                .name(associatedService.get().name)
                                                .port(new V1ServiceBackendPort()
                                                        .number(portNumber))))))));
        List<V1IngressTLS> tlsList = new ArrayList<>();
        V1IngressTLS tls = new V1IngressTLS()
                .hosts(Collections.singletonList(host))
                .secretName(name + "-tls");
        tlsList.add(tls);

        V1IngressSpec spec = new V1IngressSpec()
                .tls(tlsList)
                .rules(rules);

        ingressClassName.ifPresent(spec::ingressClassName);

        ingress.setSpec(spec);
        return ingress;
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }
}
