package com.upside_down_research.kaffinate;

import com.google.gson.Gson;
import io.kubernetes.client.openapi.models.V1Secret;

import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DockerConfigSecret extends Secret {
    private final String username;
    private final String password;
    private final String email;
    private final String server;

    /// Example usage:
    //        String username = "<username>";
    //        String password = "<personal access token>";
    //        String email = "<e-mail>";
    //        String registryServer = "https://registry.gitlab.com";
    public DockerConfigSecret(String name, String username, String password, String email, String server) {
        super(name, new HashMap<>());
        this.username = username;
        this.password = password;
        this.email = email;
        this.server = server;

    }

    protected V1Secret synthesize(RuntimeOptions options) {
        Map<String, String> thelabels = options.allLabels();
        // Authentication data

        // Docker auth configuration
        Map<String, Object> auth = new HashMap<>();
        auth.put("username", this.username);
        auth.put("password", this.password);
        auth.put("email", this.email);

        Map<String, Object> dockerConfigMap = new HashMap<>();
        dockerConfigMap.put(this.server, auth);

        Map<String, Object> dockerConfigJsonMap = new HashMap<>();
        dockerConfigJsonMap.put("auths", dockerConfigMap);

        Gson gson = new Gson();
        String dockerConfigJson = gson.toJson(dockerConfigJsonMap);

        return new V1Secret()
                .apiVersion("v1")
                .kind("Secret")
                .metadata(new io.kubernetes.client.openapi.models.V1ObjectMeta()
                        .name(this.name())
                        .namespace(options.namespace())
                        .labels(thelabels))
                .type("kubernetes.io/dockerconfigjson")
                .data(Collections.singletonMap(".dockerconfigjson", dockerConfigJson.getBytes()));
    }
}
