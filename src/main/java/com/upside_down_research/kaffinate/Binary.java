package com.upside_down_research.kaffinate;

import java.util.Map;

public final class Binary implements ConfigMapDataclass {
    private final Map<String, byte[]> data;

    public Binary(Map<String, byte[]> data) {
        this.data = data;
    }

    Map<String, byte[]> get() {
        return data;
    }
}
