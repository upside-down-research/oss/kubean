package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatefulSet extends PodManager implements Renderable {

    private int replicas;

    public StatefulSet(String name) {
        super(name);
    }

    @Override
    public String render(RuntimeOptions options) throws KaffinateRootException {

        List<String> rendered = new ArrayList<>(super.renderChain(options));

        V1StatefulSet statefulSet = synthesize(options);

        rendered.add(Yaml.dump(statefulSet));
        return String.join("\n---\n", rendered);
    }

    @NotNull
    private V1StatefulSet synthesize(RuntimeOptions options) {
        Map<String, String> podLabels = options.mutableLabels();


        V1PodTemplateSpec podTemplateSpec = getV1PodTemplateSpec(podLabels, options);

        V1StatefulSet statefulSet = new V1StatefulSet();
        statefulSet.setApiVersion("apps/v1");
        statefulSet.setKind("StatefulSet");
        statefulSet.setMetadata(
                new V1ObjectMeta()
                        .namespace(options.namespace())
                        .name(name)
                        .labels(options.allLabels(this.managerLabels.asMap()))
                        .annotations(this.managerAnnotations));
        statefulSet.setSpec(
                new V1StatefulSetSpec()
                        .replicas(replicas)
                        .selector(new V1LabelSelector().matchLabels(options.immutableLabels(this.podLabels)))
                        .template(podTemplateSpec));
        return statefulSet;
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }
    public StatefulSet withImage(String image) {
        super.withImage(image);
        return this;
    }
    public StatefulSet withReplicas(int replicas) {
        this.replicas = replicas;
        return this;
    }
    public StatefulSet withPodPort(String http, int i) {
        super.withPodPort(http, i);
        return this;
    }
    public StatefulSet withServicePort(String http, int i) {
        super.withServicePort(http, i);
        return this;
    }
}
