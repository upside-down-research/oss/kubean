package com.upside_down_research.kaffinate;

import io.kubernetes.client.custom.IntOrString;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1Service;
import io.kubernetes.client.openapi.models.V1ServicePort;
import io.kubernetes.client.openapi.models.V1ServiceSpec;
import io.kubernetes.client.util.Yaml;

import java.io.IOException;
import java.util.*;

public class Service implements Renderable {
    protected final String name;
    protected final Map<String, String> selectors;
    protected final Map<Integer, Pair<String, Integer>> portMap;

    /***
     * Create a new service
     * @param name name
     * @param selectors labels
     * @param portMap port mapping - port -> (name, targetPort)
     */
    Service(String name, Map<String, String> selectors, Map<Integer, Pair<String, Integer>> portMap) {
        this.name = name;
        this.selectors = selectors;
        this.portMap = portMap;
    }

    protected Map<String, Integer> getTargetPorts() {
        HashMap<String, Integer> ports = new HashMap<>();
        for (var entry : this.portMap.entrySet()) {
            ports.put(entry.getValue().first(), entry.getValue().second());
        }
        return ports;
    }


    V1Service synthesize(RuntimeOptions options) {
        V1Service service = new V1Service();
        service.apiVersion("v1");
        service.kind("Service");
        HashMap<String, String> thelabels = new HashMap<>(options.allLabels());

        service.metadata(new V1ObjectMeta()
                .name(this.name)
                .namespace(options.namespace())
                .labels(thelabels));

        List<V1ServicePort> ports = new ArrayList<>();
        for (var entry : this.portMap.entrySet()) {
            Pair<String, Integer> pair = entry.getValue();
            ports.add(new V1ServicePort()
                    .port(entry.getKey())
                    .name(pair.first())
                    .targetPort(new IntOrString(pair.second())));
        }

        service.spec(new V1ServiceSpec()
                .selector(this.selectors)
                .ports(ports));

        return service;
    }
    @Override
    public String render(RuntimeOptions options) {
        V1Service svc = synthesize(options);
        return Yaml.dump(svc);
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }

    public Optional<Integer> getFrontendPort(String portName) {
        for (var entry : this.portMap.entrySet()) {
            if (entry.getValue().first().equals(portName)) {
                return Optional.of(entry.getKey());
            }
        }
        return Optional.empty();
    }
}
