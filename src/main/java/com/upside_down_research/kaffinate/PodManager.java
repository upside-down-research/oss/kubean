package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public abstract class PodManager implements Renderable {
    private List<String> args;
    private List<InitContainer> initContainers;

    protected final String name;
    protected final Map<String, Integer> podPorts;
    protected boolean hasServiceAccount;
    protected Optional<String> imageName;

    @NotNull
    protected Map<String, String> managerAnnotations;

    @NotNull
    protected Labels managerLabels;
    @NotNull
    protected Labels podLabels;
    @NotNull
    protected Map<String, String> podAnnotations;
    /**
     * Optional service that can be associated with the deployment
     * services are not created by default
     * no free floating services.
     */
    protected Optional<Service> service;
    protected Map<String, Integer> servicePorts;
    protected Stack<Projectable> projectedData;
    protected Optional<Ingress> ingress;
    private Optional<String> pullSecretsName;
    private boolean _alwaysPull;

    PodManager(String name) {
        this.name = name;
        this.podPorts = new HashMap<String, Integer>();
        this.hasServiceAccount = true;
        this.managerLabels = new Labels();
        this.managerAnnotations = new HashMap<>();
        this.podLabels = new Labels();
        this.podAnnotations = new HashMap<>();
        this.service = Optional.empty();
        this.servicePorts = new HashMap<>();
        this.ingress = Optional.empty();
        this.imageName = Optional.empty();
        this.pullSecretsName = Optional.empty();
        this.initContainers = new ArrayList<>();
        this.args = new ArrayList<>();
        this._alwaysPull = false;
    }

    /***
     * the render chain renders the objects available in the object hierarchy.
     *
     * @param options
     */
    List<String> renderChain(RuntimeOptions options) throws KaffinateRootException {
        List<String> rendered = new ArrayList<>();
        V1ServiceAccount sa = null;
        if (this.hasServiceAccount) {
            sa = new ServiceAccount(this.serviceAccountName()).synthesize(options);
        }
        if (sa != null) {
            rendered.add(Yaml.dump(sa));
        }
        Service svc = createService(options);
        rendered.add(svc.render(options));
        if (this.ingress.isPresent()) {
            Ingress ing = this.ingress.get();
            ing.memoService(svc);
            rendered.add(ing.render(options));
            ing.clearMemoService();

        }
        return rendered;
    }

    protected String serviceAccountName() {
        return this.name + "-service-account";
    }

    protected Service createService(RuntimeOptions options) {

        Map<String, String> podLabels = options.mutableLabels();

        HashMap<Integer, Pair<String, Integer>> ports = new HashMap<>();
        if (!this.servicePorts.keySet().equals(this.podPorts.keySet())) {
            throw new IllegalArgumentException("Service and Pod port names must be the same strings");
        }
        for (String key : this.servicePorts.keySet()) {
            ports.put(this.servicePorts.get(key), new Pair<>(key, this.podPorts.get(key)));
        }


        return new Service(this.name, podLabels, ports);
    }

    /**
     * Pod ports are the ports that the pod listens on
     */
    public PodManager withPodPort(String name, int port) {
        this.podPorts.put(name, port);
        return this;
    }

    /**
     * Service ports are the ports that the service listens on
     */
    public PodManager withServicePort(String name, int port) {
        this.servicePorts.put(name, port);
        return this;
    }

    public PodManager withArgs(List<String> args) {
        this.args = args;
        return this;
    }

    public PodManager withPodLabels(Map<String, String> labels) {
        this.podLabels = new Labels(labels);
        return this;
    }

    public PodManager withManagerLabels(Map<String, String> labels) {
        this.managerLabels = new Labels(labels);
        return this;
    }

    public PodManager withPullSecrets(Secret secret) {
        this.pullSecretsName = Optional.of(secret.name());
        return this;
    }

    public PodManager withServiceAccount(boolean serviceAccount) {
        this.hasServiceAccount = serviceAccount;
        return this;
    }

    public PodManager withInitContainers(List<InitContainer> initContainers) {
        this.initContainers = initContainers;
        return this;
    }

    protected V1PodTemplateSpec getV1PodTemplateSpec(Map<String, String> podLabels, RuntimeOptions options) {
        List<V1Volume> volumes = new ArrayList<>();
        List<V1VolumeMount> volumeMounts = new ArrayList<>();
        if (this.projectedData != null) {
            for (Projectable p : this.projectedData) {
                if (p instanceof ConfigMap cm) {

                    String sharedName = cm.name() + "-vol";
                    V1Volume v = new V1Volume()
                            .name(sharedName)
                            .configMap(new V1ConfigMapVolumeSource().name(cm.name()));
                    volumes.add(v);
                    V1VolumeMount vm = new V1VolumeMount()
                            .name(sharedName)
                            .mountPath("/opt/" + sharedName);
                    volumeMounts.add(vm);
                }
                if (p instanceof Secret s) {
                    String sharedName = s.name() + "-vol";
                    V1Volume v = new V1Volume()
                            .name(sharedName)
                            .secret(new V1SecretVolumeSource().secretName(s.name()));

                    volumes.add(v);
                    V1VolumeMount vm = new V1VolumeMount()
                            .name(sharedName)
                            .mountPath("/opt/" + sharedName);
                    volumeMounts.add(vm);
                }
            }
        }

        V1Container mainContainer = new V1Container()
                .name(name)
                .image(imageName.orElseThrow())
                .ports(new ArrayList<>(podPorts.entrySet()).stream()
                        .map(e -> new V1ContainerPort().containerPort(e.getValue()))
                        .toList())
                .volumeMounts(volumeMounts);

        if (this.alwaysPull() ) {
            mainContainer.setImagePullPolicy("Always");
        }


        if (!this.args.isEmpty()) {
            mainContainer.args(this.args);
        }
        V1PodSpec podSpec = new V1PodSpec()
                .containers(Collections.singletonList(
                        mainContainer));

        if ( this.pullSecretsName.isPresent()) {
            podSpec.imagePullSecrets(Collections.singletonList(new V1LocalObjectReference().name(this.pullSecretsName.get())));
        }

        podSpec.volumes(volumes);

        List<V1Container> initContainers = new ArrayList<>();
        for (InitContainer ic : this.initContainers) {
            initContainers.add(ic.synthesize(options));
        }
        podSpec.initContainers(initContainers);

        if (this.hasServiceAccount) {
            podSpec.serviceAccountName(this.serviceAccountName());
        }

        return new V1PodTemplateSpec()
                .metadata(new V1ObjectMeta()
                        .labels(options.allLabels(this.podLabels.asMap()))
                        .annotations(this.podAnnotations))
                .spec(podSpec);
    }

    public boolean setAlwaysPull(boolean alwaysPull) {
        return this._alwaysPull = alwaysPull;
    }
    public boolean alwaysPull() {
        return this._alwaysPull;
    }

    public PodManager withImage(String image) {
        this.imageName = Optional.of(image);
        return this;
    }

    public PodManager withImage(String image, String tag) {
        this.imageName = Optional.of(image + ":" + tag);
        return this;
    }

    public PodManager withPodAnnotations(HashMap<String, String> annotations) {
        this.podAnnotations = annotations;
        return this;
    }


    public PodManager withDeployAnnotations(HashMap<String, String> annotations) {
        this.managerAnnotations = annotations;
        return this;
    }

    public void memo(List<ConfigMap> configMaps, List<Secret> secrets) {
        Stack<Projectable> stack = new Stack<>();
        stack.addAll(configMaps);
        stack.addAll(secrets);
        this.projectedData = stack;
    }

    public void clearMemo() {
        this.projectedData.clear();
    }

    public PodManager withIngress(Ingress ingress) {
        this.ingress = Optional.of(ingress);
        return this;
    }
}
