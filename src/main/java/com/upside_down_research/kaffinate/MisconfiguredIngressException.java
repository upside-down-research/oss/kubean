package com.upside_down_research.kaffinate;

public final class MisconfiguredIngressException extends KaffinateRootException {
    public MisconfiguredIngressException(String message) {
        super(message);
    }
}


