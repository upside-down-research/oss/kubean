package com.upside_down_research.kaffinate;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1Secret;
import io.kubernetes.client.util.Yaml;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Secret  implements Renderable, Projectable {

    private final String name;
    private final Map<String, byte[]> data;

    public Secret(String name, Map<String, byte[]> data) {
        this.name = name;
        this.data = data;
    }

    public String name() {
        return this.name;
    }

    protected HashMap<String, String> getLabels(RuntimeOptions options) {
        HashMap<String, String> thelabels = new HashMap<>();
        thelabels.putAll(options.allLabels());
        return thelabels;
    }

    protected V1Secret synthesize(RuntimeOptions options) {
        HashMap<String, String> thelabels = getLabels(options);

        return new V1Secret()
                 .apiVersion("v1")
                 .kind("Secret")
                 .metadata(new V1ObjectMeta()
                         .name(this.name)
                         .namespace(options.namespace())
                         .labels(thelabels))
                .immutable(false)
                .data(this.data);
    }

    @Override
    public String render(RuntimeOptions options) {
        V1Secret s = this.synthesize(options);
        return Yaml.dump(s);
    }

    @Override
    public void create(RuntimeOptions options) throws IOException, ApiException {

    }

    @Override
    public void delete(RuntimeOptions options) throws IOException, ApiException {

    }
}

