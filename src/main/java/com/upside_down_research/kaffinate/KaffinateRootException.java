package com.upside_down_research.kaffinate;

public abstract class KaffinateRootException extends Exception {
    public KaffinateRootException(String message) {
        super(message);
    }
}
