package com.upside_down_research.kaffinate;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

class ApplicationTest {

    @Test
    void renderEmpty() throws KaffinateRootException {
        Application app = new Application("test");
        String output = app.render(new RuntimeOptions("default"));
        org.junit.jupiter.api.Assertions.assertEquals("", output);
    }


    @Test
    void exerciseItAll() throws KaffinateRootException {

        PodManager deploy = new Deployment("test-deploy")
                .enableHpa("cpu", 80)
                .withReplicas(3)
                .withImage("httpd:2.4")
                .withPodPort("http", 80)
                .withServicePort("http", 8080);


        StatefulSet statefulSet = new StatefulSet("test-statefulset")
                .withReplicas(3)
                .withImage( "httpd:2.4")
                .withPodPort("http", 80)
                .withServicePort("http", 9090);

        PodManager pm = new Daemonset("test-daemonset")
                .withImage("httpd:2.4")
                .withPodPort("http", 80)
                .withServicePort("http", 1337);

        Application app = new Application("test-app")
                .withPodmanager(deploy)
                .withPodmanager(statefulSet)
                .withPodmanager(pm);

        ConfigMap cm1 = new ConfigMap("test-configmap", new Text(Map.of("entry", "hello world")));
        ConfigMap cm2 = new ConfigMap("test-configmap2", new Binary(Map.of("otherentry", "hello world".getBytes())));

        app.withConfigMaps(List.of(cm1, cm2));

        Secret s = new Secret("test-secret", Map.of("password", "super secret".getBytes()));
        app.addSecret(s);

        System.out.println(app.render(new RuntimeOptions("default",
                app.applicationName,
                Map.of("appName", "test-app"),
                Map.of("iteration", "1", "appVersion", "1.0.0"))));
    }
}