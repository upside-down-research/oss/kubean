package com.upside_down_research.kaffinate;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TextTest {

    @Test
    void get() {
        // exercise setup and getting of the data
        Text text = new Text(Map.of("key1","value1"));
        Map<String, String> output = text.get();
        assertEquals(1, output.size());
        assertEquals("value1", output.get("key1"));
    }
}