package com.upside_down_research.kaffinate;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class LabelsTest {

    @Test
    void asMap() {
        // Test the labels conversion to map
        Labels labels = new Labels();
        labels.withLabel("key1", "value1");
        labels.withLabel("key2", "value2");
        labels.withLabel("key3", "value3");
        Map<String, String> output = labels.asMap();
        assertEquals(3, output.size());
        assertEquals("value1", output.get("key1"));
        assertEquals("value2", output.get("key2"));
        assertEquals("value3", output.get("key3"));
    }

    @Test
    void withLabel() {
        // Test the addition of a label and the fluent interface
        Labels labels = new Labels();
        labels.withLabel("key1", "value1").withLabel("key2", "value2").withLabel("key3", "value3");
        Map<String, String> output = labels.asMap();
        assertEquals(3, output.size());
        assertEquals("value1", output.get("key1"));
        assertEquals("value2", output.get("key2"));
        assertEquals("value3", output.get("key3"));

    }

    @Test
    void merge() {
        // set up two different maps and merge them.
        Labels labels1 = new Labels();
        labels1.withLabel("key1", "value1")
                .withLabel("key2", "value2")
                .withLabel("key3", "value3")
                .withLabel("commonKey", "commonValue-One");
        Labels labels2 = new Labels();
        // Use A, B, C for label suffixes
        labels2.withLabel("keyA", "valueA").withLabel("keyB", "valueB").withLabel("keyC", "valueC")
                .withLabel("commonKey", "commonValue-Alpha");
        Labels merged = labels1.merge(labels2);
        Map<String, String> output = merged.asMap();
        assertEquals(7, output.size());
        assertEquals("value1", output.get("key1"));
        assertEquals("value2", output.get("key2"));
        assertEquals("value3", output.get("key3"));
        assertEquals("valueA", output.get("keyA"));
        assertEquals("valueB", output.get("keyB"));
        assertEquals("valueC", output.get("keyC"));
        assertEquals("commonValue-Alpha", output.get("commonKey"));
    }
}