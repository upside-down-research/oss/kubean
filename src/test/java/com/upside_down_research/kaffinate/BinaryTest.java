package com.upside_down_research.kaffinate;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BinaryTest {

    @Test
    void get() {
        // exercise setup and getting of the data
        Binary binary = new Binary(Map.of("key1","value1".getBytes()));
        Map<String, byte[]> output = binary.get();
        assertEquals(1, output.size());
        assertArrayEquals("value1".getBytes(), output.get("key1"));
    }
}